const mongoose = require("mongoose")
const quiz = require("../models/quiz");


const loadQuiz = async (req, res) => {
    // console.log("Hi");
    try{
        const {
            quizName,
            questionNumber,
        } = req.body;

        // console.log(req.body);
        // console.log(quizName+ " : "+questionNumber);
        const searchQuiz = await quiz.findOne({ quizName });

        if (!searchQuiz) {
            // console.log("Er");
            return res.send("Quiz not found");
        }
        // console.log(searchQuiz)
        const searchQuestion = searchQuiz.questions.find(question => question.qNo === questionNumber);
        // console.log(searchQuestion);
        if (!searchQuestion) {
            // console.log("Err");
            return res.send("Question not found");
        }
        // console.log(searchQuestion);
        res.send({message:"Load Sucessfull",question:searchQuestion});
    
    }
    catch(error){
        console.log("Error saving Quiz : "+error);
        return;
    }
};
module.exports = {loadQuiz};
