const mongoose = require("mongoose")
const quiz = require("../models/quiz");


const loadQuizData = async (req, res) => {
    // console.log("Hi");
    try{
        const quizName= req.body.quizName;
        // console.log("TEmp\n\n\n\n\n\n\n\nn\n\n\n\n\n\n\n\\n\n")
        // console.log(req.body);
        // console.log(req)
        console.log("Load Quiz Quiz Name Recieved "+quizName);
        const searchQuiz = await quiz.findOne({ quizName });
        
        if (!searchQuiz) {
            return res.send("Quiz not found");
        }
        const question = searchQuiz.questions.find(question => question.qNo === 1);

        if (!question) {
            return res.send("Question not found");
        }
        res.send({message:"Quiz Found",quiz:question,questionCount:searchQuiz.questionCount});
    
    }
    catch(error){
        console.log("Error saving Quiz : "+error);
        return;
    }
};
module.exports = {loadQuizData};
