const mongoose = require("mongoose");
const quizCreator = require("../models/quizCreator");
const users = require("../models/users");
const quiz = require("../models/quiz");

const createQuizStartLaunch = async (req, res) => {
    try{
        // console.log("Hellooo");
        const checkHost = await users.findOne({userName:req.body.userName});
        const checkHostId = checkHost._id;
        // console.log("checkHostId :"+checkHostId);
        quizCreator.findOne({ hostId:checkHostId })
            .populate('quizId')
            .populate('hostId')
            .then((quizCreator) => {
                // console.log(quizCreator);
                if (!quizCreator) {
                    console.error("Error Populating:");
                    res.json({message:"Error Populating"})
                    return;
                }
                const userName = quizCreator.hostId.userName;
                const quizName = quizCreator.quizId.quizName;
                console.log("Create QUiz Start Launch : "+userName+ " : "+quizName);
                res.json({userName:userName,quizName:quizName});
            })
            .catch((error) => {
                console.log("Error Finding HostID"+error);
                res.json({message:"Error Finding HostID"});
                return;
            });

    }
    catch(error){
        console.log("Error Create Quiz Start Launch : "+error);
        return;
    }
};
module.exports = {createQuizStartLaunch};
