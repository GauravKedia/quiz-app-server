const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const Users = require("../models/users");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const register = async (req, res) => {
  // console.log(req);
  const { firstName, lastName, email, password, userName } = req.body;
  console.log("Register Data Recieved : " + firstName + " " + lastName);
  const checkuser = await Users.findOne({ userName: userName });
  const checkemail = await Users.findOne({ email: email });
//   console.log(checkemail + " : " + checkuser);
  if (checkuser !== null || checkemail !== null) {
    console.log("User Already Exist");
    res.json({ message: "User Already Exist" });
  } else if (!password) {
    res.json({ message: "Password Field is Empty" });
  } else {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(password, salt);
    try {
      // console.log(hash);
      // console.log(firstName+" : "+lastName+ " : "+email+ " : "+password+" : "+userName);
      const newusers = await Users.create({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: hash,
        userName: userName,
      });
      // console.log("Data Send");
      const accesstoken = generateToken(newusers);
      const reaccesstoken = regenerateToken(newusers);
      // console.log(accesstoken);
      res.json({
        result: newusers,
        accesstoken: accesstoken,
        refreshtoken: reaccesstoken,
        message: "Registration Successfull!",
      });
    } catch (error) {
      res.json("Error Sending Register Data to Database");
    }
  }
};

const login = async (req, res) => {
  // console.log(req);
  console.log(req.body.userName);
  const newusers = await Users.findOne({ userName: req.body.userName });
  console.log(newusers);
  if (!newusers) res.json({ message: "Username Not found" });
  else {
    try {
        // console.log(req.body.password);
        // console.log(newusers.password);
      if (await bcrypt.compare(req.body.password, newusers.password)) {
        const accesstoken = generateToken(newusers);
        const reaccesstoken = regenerateToken(newusers);
        res.json({
          result: newusers,
          accesstoken: accesstoken,
          refreshtoken: reaccesstoken,
          message: "Login Successfull!"
        });
      } else {
        res.json({ message: "Invalid Username or Password" });
      }
    } catch (error) {
      res.json({ message: "Error Occured while Login" });
    }
  }
};  

const generateToken = (newusers) => {
  var token = jwt.sign({ userName: newusers.userName }, process.env.secretkey, {
    expiresIn: "2h",
  });
  return token;
};

const regenerateToken = (newusers) => {
  var token = jwt.sign(
    { userName: newusers.userName },
    process.env.resecretkey,
    {
      expiresIn: "10h",
    }
  );
  return token;
};

module.exports = { register, login };
