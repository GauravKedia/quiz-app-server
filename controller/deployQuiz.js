const mongoose = require("mongoose")
const quizCreator = require("../models/quizCreator");
const quiz = require("../models/quiz");


const deployQuiz = async (req, res) => {
    try{
        const quizName= req.body.quizName;
        
        console.log(" Deploy Quiz Name Recieved "+quizName);
        const searchQuiz = await quiz.findOne({ quizName });
        
        if (!searchQuiz) {
            return res.send("Quiz not found");
        }
        console.log("QQQ:"+quizName);
        console.log("QQQQ :"+req.body.scheduleDate);
        if(!req.body.scheduleDate){
            req.body.scheduleDate = new Date(Date.now() + 300000);
        }
        const newQuiz = await quizCreator.findOne({quizId:searchQuiz._id});
        newQuiz.schedule = req.body.scheduleDate;
        res.send({message:"Quiz Deployed"});
        await newQuiz.save();

    }
    catch(error){
        console.log("Error Deploying Quiz : "+error);
        return;
    }
};
module.exports = {deployQuiz};
