const mongoose = require("mongoose");
const quizCreator = require("../models/quizCreator");
const users = require("../models/users");
const quiz = require("../models/quiz");

const initiateCreateQuiz = async (req, res) => {
  try {
    const { userName, quizName, quizPin } = req.body;
    const user = await users.findOne({ userName: userName });
    const checkUser = await quizCreator.findOne({ hostId: user._id });
    const checkQuizName = await quiz.findOne({quizName:quizName});
    //Update this with !checkuser && !checkQuizName after development to add quiz
    if (!checkQuizName && !checkUser) {
      await quiz.create({
        hostId: user._id,
        quizName: quizName,
        questionCount: 0,
        onGoingQuestion:0,
        questions: [],
      });
      const newquiz = await quiz.findOne({
        hostId: user._id,
        quizName: quizName,
      });
      const resquizCreator = await quizCreator.create({
        hostId: user._id,
        quizId: newquiz._id,
        pin: quizPin,
        schedule: Date.now(),
        isLive: false,
      });

      if (resquizCreator) {
        res.json({ message: "Initiate Successfull!" });
      } else {
        res.json({ message: "Failed to initiate quiz creation." });
      }
    }
    else{
        res.json({message: `Quiz Already Present for this user`});
    }
  } catch (error) {
    console.error("Error initiating quiz creation:", error);
    res.json({ message: "Internal Server Error" });
  }
};
module.exports = initiateCreateQuiz;

// {
//     quizName: 'AVZ',
//     quizPin: 'SAC',
//     newAccessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyTmFtZSI6ImdhdXJhdl9rZWRpYSIsImlhdCI6MTcwNzMwMDQxNSwiZXhwIjoxNzA3MzA3NjE1fQ.XIxU0I8Sv4UMFEhIs7h_QDXd8P7yutWHuZnfO_6OFrw',
//     userName: 'gaurav_kedia'
// }
