const mongoose = require("mongoose");
const quiz = require("../models/quiz");

const deleteQuiz = async (req, res) => {
  console.log("Hi");
  try {
    const { quizName, questionNumber } = req.body;
    console.log(quizName + " : " + questionNumber);
    const deleteQuiz = await quiz.findOne({ quizName: quizName });

    if (!deleteQuiz) {
      return res.send("Quiz not found");
    }

    console.log(deleteQuiz);
    const checkQuestionNumber = deleteQuiz.questions.some(
      (question) => question.qNo === questionNumber
    );
    console.log(checkQuestionNumber);
    if (!checkQuestionNumber) {
      return res.send("Question not found");
    }

    await quiz.findOneAndUpdate(
      { quizName: quizName },
      {
        $pull: { questions: { qNo: questionNumber } },
        $inc: { questionCount: -1 },
      },
      { new: true }
    );

    console.log("Del : " + deleteQuiz.questions.length);
    for (let i = 0; i < deleteQuiz.questions.length; i++) {
      deleteQuiz.questions[i].qNo = i + 1;
    }
    await deleteQuiz.save();

    res.send({ message: "Delete Sucessfull" });
  } catch (error) {
    console.log("Error Deleting Quiz : " + error);
    return;
  }
};
module.exports = { deleteQuiz };
