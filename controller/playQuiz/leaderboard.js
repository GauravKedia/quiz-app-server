const mongoose = require("mongoose");
const quiz = require("../../models/quiz");
const players = require("../../models/player");
// const startSocket = require("./startSocket");

const leaderboard = async (req, res) => {
  try {
    const { quizName } = req.body;
    console.log("LeaderBoard Received : "+quizName);
    const currentQuiz = await quiz.findOne({ quizName:quizName });
    if (!currentQuiz) {
      return res.json({ message: "Quiz not found" });
    }
    const playersData = await players
      .find({ quizId: currentQuiz._id })
      .populate("playerId")
      .sort({score:-1});

    console.log("Players Data : "+playersData);

    const leaderboardData = playersData.map((player) => ({
      userName: player.playerId.userName,
      score: player.score,
    }));  


    console.log("Leaderboard Data: "+leaderboardData);
    return res.json(leaderboardData);
  } catch (error) {
    console.log("Leaderboard :" + error);
  }
};
module.exports = leaderboard;
