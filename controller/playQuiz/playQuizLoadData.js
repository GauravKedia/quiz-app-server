const mongoose = require("mongoose");
const users = require("../../models/users");
const quiz = require("../../models/quiz");
const player = require("../../models/player");


const playQuizLoadData = async (req,res)=>{
    try{
        const {userName,quizName} = req.body;
        console.log("Play Quiz Load Data Received : "+ userName + " : " + quizName)
        
        const newQuiz = await quiz.findOne({quizName:quizName});
        const newUser= await users.findOne({userName:userName});
        
        if(!newQuiz || !newUser){
            return res.json({message:"Play Quiz Load Data Not Found"});
        }
        
        const newPlayer  = await player.findOne({quizId:newQuiz._id,playerId:newUser._id});
        
        if(!newPlayer){
            return res.json({message:"Player of Play Quiz Not Found"});
        }

        console.log("Backend Play QUiz Loa Data Sending :"+userName +" : "+newPlayer.score);
        
        res.json({message:"Play Quiz Data Found!",userName:userName,score:newPlayer.score});
    }
    catch(error){
        console.log("Server Error Play QUiz Load Data:"+error);
    }
}
module.exports = playQuizLoadData;