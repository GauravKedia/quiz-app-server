const mongoose = require("mongoose");
const users = require("../../models/users");
const quiz = require("../../models/quiz");
const player = require("../../models/player");

const checkAnswer = async (req, res) => {
  try {
    const { userName, quizName, optionValue, currentQNo } = req.body;
    console.log("Check Answer Received : "+ userName + " : " + quizName + " : " + optionValue + " : " + currentQNo);

    const currentQuiz = await quiz.findOne({ quizName:quizName });

    if (!currentQuiz) {
      return res.json({ message: "Quiz not found" });
    }

    const currentQuestion = currentQuiz.questions.find(
      (question) => question.qNo === currentQNo
    );
    
    if (!currentQuestion) {
      return res.json({ message: "Question not found" });
    }

    const selectedOption = currentQuestion.options.find(
      (opt) => opt.optionValue === optionValue
    );
    if (!selectedOption) {
      return res.json({ message: "Option not found" });
    }
    console.log("Selected Option : "+selectedOption);

    if (selectedOption.correct) {
      const newPlayer = await users.findOne({ userName: userName });
      console.log("New Player : "+newPlayer);
      console.log("New Player Id : " + newPlayer._id);
      console.log("Current Quiz Id: "+currentQuiz._id);
      const currentPlayer = await player.findOne({
        playerId: newPlayer._id,
        quizId: currentQuiz._id,
      });
      console.log("Current Player : " + currentPlayer);
      if (!currentPlayer) {
        return res.json({ message: "Player not found" });
      }
      currentPlayer.score += currentQuestion.points;
      await currentPlayer.save();
      return res.json({ message: "Correct answer" });
    } 
    else {
      return res.json({ message: "Incorrect answer" });
    }
  } catch (error) {
    console.log("Check Answer Error :" + error);
  }
};
module.exports = checkAnswer;
