const players = require("../../models/player");
const users = require("../../models/users");
const quizzes = require("../../models/quiz");
const quizCreator = require("../../models/quizCreator");
const socketAuthentication = require("../../middleware/socketAuthentication");

const startSocket = (io) => {
  io.use(socketAuthentication);
  io.on("connection", (socket) => {
    console.log("Socket connected: " + socket.id);

    socket.on("check_game_started", async ({ quizName },callback) => {
      console.log("Socket : check_game_started : quizname :" + quizName);
      try {
        if (!socket.user || !socket.user.userName) {
          console.log("Socket User information not available");
          callback("Socket User information not available");
          return;
        }
        const newUser = await users.findOne({ userName: socket.user.userName });
        const newQuiz = await quizzes.findOne({ quizName: quizName });

        if (newUser && newQuiz) {
          const checkPlayer = await players.findOne({
            playerId: newUser._id,
            quizId: newQuiz._id,
          });
          console.log("Check PLayer : " + checkPlayer);
          if (!checkPlayer) {
            await players.create({
              playerId: newUser._id,
              quizId: newQuiz._id,
              score: 0,
            });
          }

          const checkSchedule = await quizCreator.findOne({
            quizId: newQuiz._id,
          });

          if (checkSchedule) {
            // Joining Room
            socket.join(quizName);
            console.log("Joined Room :"+quizName);

            if (checkSchedule.isLive === false) {

              console.log("Check Schedule is Live : "+ checkSchedule.isLive);

              await quizCreator.findOneAndUpdate(
                { quizId: newQuiz._id },
                { isLive: true }
              )

              console.log("Check Schedule is Live Updated : "+checkSchedule.isLive);


              if (Date.now() > checkSchedule.schedule.getTime()) {
                console.log("Quiz Already started!");

                elapsedTime = Date.now() - checkSchedule.schedule.getTime();

                console.log("Elapsed Time after quiz start : " + elapsedTime);

                let currentQuestion = 1;
                let totalQuestionTime = 0;
                let waiting_time = 0;

                console.log("Total Question Count : "+ newQuiz.questionCount);
                
                for (let i = 0; i < newQuiz.questions.length; i++) {
                  const question = newQuiz.questions[i];
                  console.log("Each Instance of Question Skip : CQ :" + currentQuestion + ", TQ :"+totalQuestionTime+ "WT : " +waiting_time + " Elapsed Time : "+elapsedTime );
                  totalQuestionTime += question.timeOfQuestion * 1000;
                  if (elapsedTime > totalQuestionTime) {
                    currentQuestion++;
                  } else {
                    waiting_time = totalQuestionTime - elapsedTime;
                    break;
                  }
                  if (currentQuestion >= newQuiz.questionCount) {
                    newQuiz.onGoingQuestion = newQuiz.questionCount;
                    await newQuiz.save();
                    callback("Quiz Over");
                    return;
                  }
                }
                
                console.log("Waiting Time is : "+ waiting_time);

                newQuiz.onGoingQuestion = currentQuestion;
                console.log("If New Quiz On Going Question : " +newQuiz.onGoingQuestion);
                await newQuiz.save();
                // await quizCreator.findOneAndUpdate(
                //   { quizId: newQuiz._id },
                //   { isLive: true }
                // )
                console.log("On Going Question : " + newQuiz.onGoingQuestion);
                

                setTimeout(async () => {
                  console.log("Check Game Started Returning : Sending Question!");
                  callback("Sending Question!");
                  return;
                }, waiting_time);

              } else {

                console.log("Quiz has not started yet");
                const waiting_time = checkSchedule.schedule.getTime() - Date.now();
                console.log("Waiting Time to Quiz Start : "+waiting_time);

                setTimeout(async () => {
                  console.log("Wait Time Over Sending Questions !");
                  newQuiz.onGoingQuestion = 0;
                  console.log("Else New Quiz On Going Question : " +newQuiz.onGoingQuestion);
                  await newQuiz.save();
                  console.log("Check Game Started Returning : Sending Question!");
                  callback("Sending Question!");
                  return;
                }, waiting_time);
              }
            }
            else{
              if(newQuiz.onGoingQuestion>=newQuiz.questionCount){
                callback("Quiz Over");
                return;
              }


              
              callback("Quiz Already Initiated");
            }
          }
          else{
            callback("Quiz Creator Data Not Found");
          }
        }
        else{
          callback("New User of New Quiz Player not found");
        }
      } catch (error) {
        console.log("Error in Socket: " + error);
      }
    });



    socket.on("request_question", async (quizName,callback) => {
      try {
        console.log("Request Question Received : " + quizName);
        const newQuiz = await quizzes.findOne({ quizName });
        if (!newQuiz) {
          console.log("Quiz not found!");
          callback("Quiz not found!");
          return;
        }

        // const newUser = await users.findOne({ userName: socket.user.userName });
        // const checkPlayer = await players.findOne({
        //   playerId: newUser._id,
        //   quizId: newQuiz._id,
        // });

        const questions = newQuiz.questions;
        let questionInterval;

        const emitQuestion = async () => {
          console.log("Emit Question Ongoing Question : " + newQuiz.onGoingQuestion);
          if (newQuiz.onGoingQuestion >= questions.length) {
            clearInterval(questionInterval);
            io.to(quizName).emit("receive_question", {
            message: "Question Over!",
          });
          return;
          }
          const currentQuestion = questions[newQuiz.onGoingQuestion];
          const optionsWithoutCorrect = currentQuestion.options.map(
            (option) => ({
              optionNo: option.optionNo,
              optionValue: option.optionValue,
            })
          );
          io.to(quizName).emit("receive_question", {
            questionInfo: {
              qNo: currentQuestion.qNo,
              qValue: currentQuestion.qValue,
              timeOfQuestion: currentQuestion.timeOfQuestion,
              points: currentQuestion.points, 
              options: optionsWithoutCorrect,
            },
            message: "Question Sent!",
            quizName: quizName,
          });
          const currentInterval = currentQuestion.timeOfQuestion * 1000;
          newQuiz.onGoingQuestion++;
          await newQuiz.save();

          clearInterval(questionInterval);
          questionInterval = setInterval(emitQuestion, currentInterval);
          console.log("Updated Interval and Question NO: " + currentInterval + " : " + newQuiz.onGoingQuestion);
        };

        emitQuestion();
        
      } catch (error) {
        console.log("Request Question: " + error);
        callback("Request Question : "+error);
        return;
      }
    });

    socket.on("on_disconnect", async ({ quizName },callback) => {
      console.log("Socket disconnected: " + socket.id);
      const room = io.sockets.adapter.rooms[quizName];
      if (!room || room.length === 0) {
        try {
          await quizCreator.findOneAndUpdate(
            { quizName },
            { isLive: false }
          );
          console.log("Socket Disconnected for : " + quizName);
          callback("Socket Disconnect and is Live Changed");
        } catch (error) {
          console.error("Error updating is Live in Socket:", error);
        } 
      }
    });
  });
};

module.exports = startSocket;
