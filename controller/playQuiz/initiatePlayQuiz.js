const mongoose = require("mongoose");
const quizCreator = require("../../models/quizCreator");
const users = require("../../models/users");
const quiz = require("../../models/quiz");
// const startSocket = require("./startSocket");


const initatePlayQuiz = async (req,res)=>{
    try{
        const {userName,quizName,quizPin} = req.body;
        console.log("Initiate Play Quiz Received : "+ userName + " : "+ quizName + " : "+quizPin)
        const newQuiz = await quiz.findOne({quizName:quizName});
        if(!newQuiz){
            return res.json({message:"Quiz Name Not Found"});
        }
        const newQuizCreator  = await quizCreator.findOne({quizId:newQuiz._id});
        if(!newQuizCreator){
            return res.json({message:"Quiz Creator Not Found"});
        }
        if(newQuiz.quizName!==quizName || newQuizCreator.pin!==quizPin){
            return res.json({message:"Invalid Quiz Name of Quiz Pin"});
        }
        const newHostId = await users.findOne({userName:userName});
        if(newHostId){
            console.log("Quiz Access Approved");
            return res.json({message:"Quiz Access Approved!"})
        }
        else{
            return res.json({message:"Quiz Access Failed!"})
        }
    }
    catch(error){
        console.log("Server Error Initiate Quiz Play :"+error);
        res.json({message: "Server Error Initiate Quiz Play"})
    }
}
module.exports = initatePlayQuiz;