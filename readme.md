# Quiz App

Welcome to the Quiz App project! This web application allows users to create and participate in quizzes. The project is divided into a React.js frontend and a Node.js backend using MongoDB as the database.


## Project Overview

The Quiz App is a web application that enables users to:

- Create quizzes
- Join existing quizzes
- Participate in quizzes
- View quiz results

The project is structured with a React.js frontend in the `client` folder and a Node.js backend in the `server` folder.
For detailed documentation, check the `documentations` folder.

## Folder Structure

- `server`: Houses the Node.js backend code.
- `client`: Holds the React.js frontend code with the following structure:
  - `public`: Public assets and HTML template.
  - `src`: React.js source code.
    - `pages`: Individual pages or views.

## Start

1. Navigate to the `client` folder using the terminal:

```bash
cd client
```

2. Install npm packages and start

- Development Mode

```bash
npm install
npm start
```

- Production Mode

```bash
npm install --production
npm start
```

- Dev Mode

```bash
npm run dev
```
