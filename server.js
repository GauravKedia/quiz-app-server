const express = require('express');
const app = express();
const cors = require("cors")

const http = require('http');
const {Server} = require("socket.io");
const mongoose = require('mongoose'); 
const authRouter = require("./routes/authRouter");
const createQuizRouter = require("./routes/createQuizRouter")
const playQuizRouter = require("./routes/playQuizRouter");
require('dotenv').config();

app.use(express.json());
app.use(cors());

const server = http.createServer(app);
const io = new Server(server,{
  cors:{
    origin: "*",
    methods: ["GET", "POST"],
    credentials: true
  }
});

require("./controller/playQuiz/startSocket")(io);


const database ="quizgame"
const url = `mongodb+srv://${process.env.Username}:${process.env.Password}@cluster0.vwqvt9o.mongodb.net/${database}`

mongoose.connect(url)
mongoose.connection.on("connected",()=>{
    console.log("Server Connected to Database")
})
mongoose.connection.on("disconnected",()=>{
    console.log("Server Database Disconnected")
})
mongoose.connection.on("error",(error)=>{
  console.log("Error Connecting Database : "+error)
})

app.use('/auth',authRouter);
app.use("/create_quiz",createQuizRouter); 
app.use("/play_quiz",playQuizRouter);



server.listen(process.env.ServerPort, ()=>{console.log("Server Connected to port :" + process.env.ServerPort)});
