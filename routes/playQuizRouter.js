const express = require("express");
const app = express();

app.use(express.json());

const router = express.Router();
const initiatePlayQuiz = require("../controller/playQuiz/initiatePlayQuiz");
const playQuizLoadData = require("../controller/playQuiz/playQuizLoadData");
const {authenticateToken} = require("../middleware/authentication");
const checkAnswer = require("../controller/playQuiz/checkAnswer");
const leaderboard = require("../controller/playQuiz/leaderboard");
// const {startSocket} = require("../controller/playQuiz/startSocket");
router.post('/initiate',authenticateToken,(req,res) => {
    console.log("Router /initiate Authenticated")
    initiatePlayQuiz(req,res);
});

router.post('/playQuizLoadData',authenticateToken,(req,res)=>{
    console.log("Router /playQuizLoadData Authenticated")
    playQuizLoadData(req,res);
})

router.post('/checkAnswer',authenticateToken,(req,res)=>{
    console.log("Router /checkAnswer Authenticated")
    checkAnswer(req,res);
} )

router.post('/leaderboard',authenticateToken,(req,res)=>{
    console.log("Router /leaderboard Authenticated")
    leaderboard(req,res);
} )
// router.post('/connectSocket',authenticateToken,(req,res)=>{
//     startSocket();
// })


module.exports = router;