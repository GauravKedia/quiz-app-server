const express = require("express");
const app = express();
app.use(express.json());
const router = express.Router();
const initiateCreateQuiz = require("../controller/initiateCreateQuiz");
const {createQuizStartLaunch} = require("../controller/createQuizStartLaunch");
const {authenticateToken} = require("../middleware/authentication");
const {saveCreateQuiz} = require("../controller/saveCreateQuiz");
const {loadQuiz}  = require("../controller/loadQuiz");
const {deleteQuiz} = require("../controller/deleteQuiz");
const {loadQuizData} = require("../controller/loadQuizData")
const {deployQuiz} = require("../controller/deployQuiz");

router.post('/initiate',authenticateToken,(req,res) => {
    initiateCreateQuiz(req,res);
});

router.post('/startLaunch',authenticateToken,(req,res) => {
    createQuizStartLaunch(req,res);
});

router.post('/saveQuiz',authenticateToken,(req,res) => {
    saveCreateQuiz(req,res);
});
router.post('/addQuiz',authenticateToken,(req,res) => {
    addQuiz(req,res);
});
router.post('/loadQuiz',authenticateToken,(req,res) => {
    // console.log("Hi"+req.body.questionNumber);
    loadQuiz(req,res);
});

router.post('/deleteQuiz',authenticateToken,(req,res) => {
    // console.log("QUestion :" +req.body.questionNumber);
    deleteQuiz(req,res);
});

router.post('/loadQuizData',authenticateToken,(req,res) => {
    // console.log("QuizName :" +req.body.quizName);
    loadQuizData(req,res);
});

router.post('/deployQuiz',authenticateToken,(req,res) => {
    // console.log("QuizName :" +req.body.quizName);
    deployQuiz(req,res);
});

// router.post('/launch',authenticateToken,(req,res) => {
//     createQuizLaunch(req,res);
// });  

module.exports = router;