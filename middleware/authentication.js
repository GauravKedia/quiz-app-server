require("dotenv").config();
const jwt = require("jsonwebtoken");

const authenticateToken = (req, res, next) => {
  // console.log(req);
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  // console.log(authHeader);
  // console.log(token);
  if (token === null) {
    res.json({ message: "Please Login Again" });
    return;
  } else {
    jwt.verify(token, process.env.secretkey, (error, user) => {
      if (error) {
        res.json({ message: "Please Login Again." });
        return;
      } else {
        // console.log("USer :")
        // console.log(user);
        if (regenerateToken(req, res)) {
          res.json({ message: "Invalid Refresh Token" });
          return;
        }
        req.user = user;
        console.log("Logged In");
        next();
      }
    });
  }
};

const regenerateToken = (req, res) => {
  const authHeader = req.headers["authorization"];
  const refreshToken = authHeader && authHeader.split(" ")[2];
  if (refreshToken == null) {
    return false;
  } else {
    jwt.verify(refreshToken, process.env.resecretkey, (error, user) => {
      if (error) {
        return false;
      } else {
        // console.log(user.userName);
        req.body.newAccessToken = generateToken({ userName: user.userName });
        req.body.userName = user.userName;
        return true;
      }
    });
  }
};

const generateToken = (newusers) => {
  // console.log("Generate Token Recieved: " + newusers);
  var token = jwt.sign(newusers, process.env.secretkey, {
    expiresIn: "2h",
  });
  // console.log("Token : " + token);
  return token;
};
module.exports = { authenticateToken, regenerateToken };
