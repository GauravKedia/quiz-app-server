const mongoose = require("mongoose");

const quizzes = new mongoose.Schema({
  quizName: {
    type: String,
    required: true,
  },
  hostId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  questionCount: {
    type: Number,
    required: true,
  },
  onGoingQuestion:{
    type:Number,
    default:0,
  },
  questions: [{
    qNo: {
      type: Number,
      required: true,
    },
    qValue: {
      type: String, 
      required: true,
    },
    timeOfQuestion: {
      type: Number,
      required: true,
    },
    points:{
      type:Number,
      required:true
    },
    options: [{
      optionNo: {
        type: Number,
        required: true,
      },
      optionValue: {
        type: String,
        required: true,
      },
      correct: {
        type: Boolean,
        default: false,
      },
    }],
  }],
});

module.exports = mongoose.model("quizzes", quizzes);
